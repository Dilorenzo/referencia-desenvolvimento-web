navbarBars = document.querySelector('.navbar-bars');
sideBar = document.querySelector('.sidebar');

if (navbarBars) {
    navbarBars.addEventListener('click', () => {
        sideBar.classList.toggle('open');
    });
}

document.addEventListener("DOMContentLoaded", () => {
    const form = document.querySelector('.needs-validation');    
    if (form) {        
        form.addEventListener('submit', (e) => {
            if (!e.target.checkValidity()) {
                e.preventDefault();
                e.stopPropagation();
            }

            e.target.classList.add('was-validated');
        });
    }
});